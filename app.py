import time,sys
import logging, threading
#from logentries import LogentriesHandler
import urllib2, re, psycopg2
from urlparse import urlparse
from telegram.ext import MessageHandler, Filters, Updater, CommandHandler



# set up logging

#LOGENTRIES_TOKEN = os.environ['LOGENTRIES_CACHER_TOKEN']
LOGGER = logging.getLogger('999-bot')
DEBUG_LEVEL = os.environ['DEBUG_LEVEL']
if DEBUG_LEVEL == 'DEBUG':
    logging.basicConfig(level=logging.DEBUG)
elif DEBUG_LEVEL == 'ERROR':
    logging.basicConfig(level=logging.ERROR)
elif DEBUG_LEVEL == 'INFO':
    logging.basicConfig(level=logging.INFO)
elif DEBUG_LEVEL == 'WARNING':
    logging.basicConfig(level=logging.WARNING)
else:
    logging.basicConfig(level=logging.NOTSET)
#LOGGER.addHandler(LogentriesHandler(LOGENTRIES_TOKEN))

# database stuff
#three9_db = '999_listings.db'
DATABASE_URL = os.environ['DATABASE_URL']
BOT_TOKEN = os.environ['BOT_TOKEN']
HEROKU_APP_URL = os.environ['HEROKU_APP_URL']

def clear_db(mac):
    # remove all user data from db for testing purposes
    conn = None
    try:
        conn = psycopg2.connect(DATABASE_URL)
        c = conn.cursor()
        c.execute("DELETE FROM users")
        c.execute("DELETE FROM requests")
        c.execute("DELETE FROM results")
        conn.commit()
        c.close()
    except (Exception, psycopg2.DatabaseError) as err:
        errmsg = ('exception:"%s" was raised ' % (err))
        LOGGER.exception(errmsg)
        return
    finally:
        if conn is not None:
            conn.close()
#

def remove_requests_for_user(user_id):
        # go through request table and remove all requests for current user
        conn = psycopg2.connect(DATABASE_URL)
        c = conn.cursor()
        c.execute("DELETE FROM requests WHERE user_id=%s", (user_id,))
        conn.commit()
        c.close()
#
def get_data(mac):
    conn = None
    try:
        conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()
        c.execute("SELECT * FROM users")
        result = cur.fetchone()
        cur.close()
        if result is not None:
            return result[0]
    except (Exception, psycopg2.DatabaseError) as err:
        errmsg = ('exception:"%s" was raised when trying to read values from database <%s> with query <%s> ' % (
        err, PG_DATABASE, query))
        pub.publish_msg(msg=json.dumps({'error': errmsg}), routing_key=AMQP_ERROR_QUEUE)
        LOGGER.exception(errmsg)
        return
    finally:
        if conn is not None:
            conn.close()
#

def stop(bot, update, chat_data):

    user_info = update.message.from_user
    username = user_info['first_name']
    user_id = user_info['id']

    update.message.reply_text('OK,'+username+' I will stop all searches and leave you alone!')

    # go through request table and remove all requests for current user
    remove_requests_for_user(user_id)

    # stop the job for curent user
    job = chat_data['job']
    job.schedule_removal()
    del chat_data['job']
    print '=== STOPPING JOB FOR USER: '+username

#

def start(bot, update):

    user_info = update.message.from_user
    username = user_info['first_name']
    user_id = user_info['id']
    chat_id = user_id
    lang = user_info['language_code']
    payment = 'demo' # do something with this later
#

    if user_exists(user_id,username):
        bot.send_message(chat_id, text='Hey, ' + username + ' ,Welcome back! ')
        bot.send_message(chat_id,text=username + ', please send me a link to page with your search results from 999.md!')
        print '\nuser',user_id,'exists'
    else:
        # is a new user
        add_user(user_id, username, lang, payment)  # add user to the users table
        bot.send_message(chat_id, text='Hey, ' + username + ' ,Welcome! I can help you track new listings on 999.md! ')
        bot.send_message(chat_id,text=username + ', please send me a link to page with your search results from 999.md!')
#

def parse_message(bot, update, job_queue, chat_data):

    # in loop
    # query requests table
    # pull request page contents for each entry
    # parse results
    # search database for each result
    # if not existing, alert and add to db
    # alternatively pull result page and extract post date, compare with today's date

    text = update.message.text
    chat_id = update.message.chat_id
    message_id = update.message.message_id
    user_info = update.message.from_user

    username = user_info['first_name']
    user_id =  user_info['id']
    lang = user_info['language_code']
    payment = 'demo' # do something with this later


    if user_exists(user_id,username):
        LOGGER.info('user:%s exists' % user_id)
    else:
        # is a new user
        LOGGER.info('detected new user:%s' % user_id)
        bot.send_message(chat_id, text='Hey, '+username+' ,Welcome! I can help you track new listings on 999.md! ')
        add_user(user_id,username,lang,payment) # add user to the users table

    # send usage instructions

    if not is_url(text):
        bot.send_message(chat_id, text=username+', please send me a link to page with your search results from 999.md!')
    else:
        # if it looks like a link, check if it's a valid link
        if is_valid_link(text):
            #if not is_valid_category(text):
                #bot.send_message(chat_id, text="I'm sorry, I can only search for new car listings for now :(")
                #return
            request_url = text
            request_id = message_id
            LOGGER.info('message_id:%s' % message_id)
            if request_exists(user_id, request_url) == True:
                # such request already exists, tell the user about that
                bot.send_message(chat_id, text=username+', you have already posted this request, I will let you know about new results!')
            else:
                bot.send_message(chat_id, text="Awesome! I'll take a look and let you know when a new listing is posted! Hold on!")
                # add new request id with user_id and request_url
                add_request(user_id, request_url, request_id)
                store_current_listings()

                # set up a repeating job that will check for new listings
                context = [request_url, user_id, request_id, username]
                interval = 5

                job = job_queue.run_repeating(check_listings, interval, context=context)
                chat_data['job'] = job

        else:
            bot.send_message(chat_id, text='Oops, this is not a valid URL!')
            bot.send_message(chat_id, text='Please send me a link to page with your car search results from 999.md!')
            return
    #
#

def check_listings(bot, job):

    request_url = job.context[0]
    user_id = job.context[1]
    request_id = job.context[2]
    username = job.context[3]
    chat_id = user_id

    LOGGER.info("checking for new listings for user:%s, for request_id:%s" % (username, request_id))

    new_listings = get_new_listings(request_url, user_id, request_id)
    if new_listings:
        bot.send_message(chat_id,text='Hey ' + username + ', good news! I found some new listings for you, take a look!')
        for new in new_listings:
            LOGGER.info( 'new listing:' % new )
            bot.send_message(user_id, text=new)
        bot.send_message(chat_id, text="That's all for now, but I'll keep loking.")
#

def store_current_listings():

    conn = psycopg2.connect(DATABASE_URL)
    c = conn.cursor()
    c.execute("SELECT * FROM requests")
    fetch_result = c.fetchall()
    for row in fetch_result:
        user_id = row['user_id']
        request_url = row['request_url']
        request_id = row['request_id']

        # parse page and get all current listings
        listing_ids = get_listing_ids(request_url)
        if listing_ids:
            LGGER.info('storing the following listings' % listing_ids)
            for listing_id in listing_ids:
                c.execute("INSERT INTO results VALUES %s,%s,%s", (user_id, listing_id, request_id))
                updated_rows = cur.rowcount
                conn.commit()
                c.close()
                LOGGER.debug('inserted %s rows in results table' % updated_rows)
            conn.close()
#

def get_new_listings(request_url, user_id, request_id ):


    conn = psycopg2.connect(DATABASE_URL)
    c = conn.cursor()

    new_listings=[]
    lang = get_page_lang(request_url)
    url_base = 'https://999.md/'+lang+'/'

    # parse page and get all current listings
    listing_ids = get_listing_ids(request_url)
    user_id = int(user_id)

    if not listing_ids:
        LOGGER.info('no listing ids found in page')
        return
    for listing_id in listing_ids:

        listing_id = int(listing_id)
        c.execute("SELECT listing_id FROM results WHERE user_id=%s AND listing_id=%s", (user_id, listing_id))
        fetch_result = c.fetchone()
        #print 'fetch result:', fetch_result
        if fetch_result:
            LOGGER.debug('listing:%s exists' % listing_id)
            pass
        else:
            # means this is a new listing, notity user about that and ad it to table
            LOGGER.info( 'new listing:%s' % listing_id)
            c.execute("INSERT INTO results VALUES %s,%s,%s", (user_id, listing_id, request_id))
            conn.commit()
            c.close()
            full_url = url_base + str(listing_id)
            new_listings.append(full_url)

    c.execute("SELECT * FROM results")
    #print 'results table contents:', c.fetchall()
    conn.close()

    if new_listings:
        return new_listings
    else: return None

#


def get_page_lang(request_url):
    page_contents = urllib2.urlopen(request_url).read()
    lang = re.search('/(..)/\d+', page_contents).group(1) # get language
    return lang
#

def get_listing_ids(request_url):

    page_contents = urllib2.urlopen(request_url).read()
    listing_ids = re.findall('/../(\d+)"', page_contents) # find all listing ids
    # print 'listing array:', listing_ids
    LOGGER.debug( 'found:%s # of listings on request page' % len(listing_ids))
    return listing_ids
#

def get_listing_ids1(request_url):

    listing_ids = []
    page_contents = urllib2.urlopen(request_url).read()
    search_string_start = 'ads-list-photo-item-title "> <a href="'
    search_string_end = '"'

    positions = [m.start() for m in re.finditer(search_string_start, page_contents)] #find all indices of occurences of start string

    if not len(positions) > 0: return
    # check if stuff was found
    for p in positions:
        #print p
        block = page_contents[p + 42:p + 55]
        i = block.find('"')
        id = block[:i]
        if id.isdigit():
            listing_ids.append(id)
    print 'found', len(listing_ids), ' listings on request page'
    #print 'listing array:', listing_ids
    return listing_ids
#

def request_exists(user_id, request_url):

    # check if this is a new request
    conn = psycopg2.connect(DATABASE_URL)
    c = conn.cursor()
    c.execute("SELECT * FROM requests")
    fetch_result = c.fetchall()
    c.close()
    conn.close()


    LOGGER.debug('contents of requests table:%s ' % string(fetch_result))

    for row in fetch_result:

        if row[1] == request_url:
            LOGGER.debug('request exists under id' % row[0])
            return True
    LOGGER.debug('request doesnt exist')
    return False

#

def add_request(user_id, request_url, request_id):
    conn = psycopg2.connect(DATABASE_URL)
    c = conn.cursor()
    LOGGER.info('adding new request for  user_id:%s, url:%s, with id:%s' % (user_id, request_url, request_id))
    c.execute("INSERT INTO requests VALUES %s,%s,%s", (request_id, request_url, user_id)) # setting request_id to unique, causes a weird bug
    conn.commit()
    c.close()
    conn.close()
#

def add_user(id,name,lang,payment):
    # add new user to the users table
    LOGGER.info('adding new user: %s with id:%s' % (name,id))
    conn = psycopg2.connect(DATABASE_URL)
    c = conn.cursor()
    c.execute("INSERT INTO users VALUES %s,%s,%s,%s", (id,name,lang,payment))
    conn.commit() # Save (commit) the changes
    c.close()
    conn.close()
#
def user_exists(id,name):
    # query users table to check if this is a new user
    conn = psycopg2.connect(DATABASE_URL)
    c = conn.cursor()
    c.execute("SELECT user_id FROM users WHERE user_id=%s AND user_name=%s", (id,name))
    fetch_result = c.fetchone()
    c.close()
    conn.close()
    if fetch_result:
        return True
    else: return False
#

def is_url(text):
    # check if msg received is a url
    r = urlparse(text)
    if r.scheme == 'https':
        if r.netloc != None and r.query != None:
            return True
#


def is_valid_category(text):
    r = urlparse(text)
    e = r.path.split('/')
    if e[3] == 'transport' and e[4] == 'cars':
        return True
    else: return False
#


def is_valid_link(text):
    r = urlparse(text)
    if r.scheme == 'https':
        if r.netloc == '999.md':
            e = r.path.split('/')
            if len(e) > 1 and e[2] == 'list' and r.query != None:
                logger.debug('got valid url:', text)
                #also make a get request and see if page is valid
                url = text
                return True
#


def help(bot, update):
    update.message.reply_text("Use /start to test this bot.")
#


def error(bot, update, error):
    LOGGER.critical('Update "%s" caused error "%s"' % (update, error))
#


def main():
    # set up bot objects
    updater = Updater(token=BOT_TOKEN)
    dp = updater.dispatcher

    # add a message handler
    msg_handler = MessageHandler(Filters.text, parse_message, pass_job_queue=True, pass_chat_data=True)
    dp.add_handler(msg_handler)

    dp.add_handler(CommandHandler("stop", stop, pass_chat_data=True))
    dp.add_handler(CommandHandler("start", start, ))
    updater.dispatcher.add_error_handler(error)
    updater.dispatcher.add_handler(CommandHandler("help", help))

    PORT = int(os.environ.get('PORT', '5000'))
    updater.start_webhook(listen="0.0.0.0",
                          port=PORT,
                          url_path=TELEGRAM_TOKEN)

    andys_bot_url = HEROKU_APP_URL
    updater.bot.set_webhook(andys_bot_url + "/" + TELEGRAM_TOKEN)
    updater.idle()
#

if __name__ == '__main__':
    main()
